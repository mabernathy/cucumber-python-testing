To start:

Make sure you have both Python and Pip installed. To check:

$ python --version
Python 2.7.*

$ pip --version
pip 1.5.6 from /Library/Python/2.7/site-packages (python 2.7)

If you do not have Python, download it here: https://www.python.org/
If you do not have pip, try installing it via this command
$ easy_install pip
If this doesn't work. You're on your own: https://www.google.com

Install virtualenv
$ pip install virtualenv

Note: installing virtualenv may require sudo

Create the test code environment:
$ ./createTestEnv

This should result in a folder being created, ./cuke-python-env/

Now you should be good to run tests

Running tests:

To run the tests, just run the runTests script:
$ ./runTests

The runTests script is just a wrapper script for Behave (see https://pythonhosted.org/behave/behave.html)
so any command line args that work with Behave will work with the runTests script.

Want to run in dry-run mode, to get the skeleton steps you need to implement?
$ ./runTests -d
-OR-
$ ./runTests --dry-run

Want to only run tests with a specific tag (like @current?)
$ ./runTests -t @current
-OR-
$ ./runTests --tags @current

Want to stop running tests after encountering the first failing step?
$ ./runTests -stop