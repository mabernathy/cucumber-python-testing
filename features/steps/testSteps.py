import requests

@given('I am an unauthenticated user')
def step_impl(context):
    # This is a no-op. Nothing is needed to not be authenticated
    assert 1

@when('I request the Public API endpoint "{endpoint}"')
def step_impl(context, endpoint):
    uri = context.properties['publicapi_host'] + endpoint
    context.response = requests.get(uri)

@then('I get a response code of "{code:d}"')
def step_impl(context, code):
    error_string = 'Expected response code of {0}, Actually got {1}'.format(code, context.response.status_code)
    assert context.response.status_code == code, error_string
