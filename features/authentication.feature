Feature: Authentication

    Scenario Outline: Deny Unauthenticated Requests
        Given I am an unauthenticated user
        When I request the Public API endpoint <endpoint>
        Then I get a response code of "401"
    Examples:
    | endpoint |
    | "/api/v1/analytics/alerting/alerts" |
    | "/api/v1/analytics/alerting/alerts/unread" |
    | "/api/v1/analytics/alerting/alerts/{alertId}" |
    | "/api/v1/analytics/alerting/rules" |
    | "/api/v1/analytics/alerting/rules/{ruleId}" |
#    | "/api/v1/analytics/query" | # only supports POST
    | "/api/v1/analytics/reporting/metadata" |
    | "/api/v1/analytics/reporting/reportformats" |
    | "/api/v1/analytics/reporting/schedules" |
    | "/api/v1/analytics/reporting/schedules/{scheduleId}" |
    | "/api/v1/analytics/reporting/schedules/{scheduleId}/history" |
    | "/api/v1/analytics/reporting/schedules/{scheduleId}/history/{runId}" |
    | "/api/v1/analytics/reporting/timeperiods" |
    | "/api/v1/analytics/reporting/{reportId}/metadata" |
    | "/api/v1/architect/ivrs" |
    | "/api/v1/architect/ivrs/{ivrId}" |
    | "/api/v1/architect/prompts" |
    | "/api/v1/architect/prompts/{promptId}" |
    | "/api/v1/architect/prompts/{promptId}/resources" |
    | "/api/v1/architect/prompts/{promptId}/resources/{resourceId}" |
    | "/api/v1/authorization/licenses" |
    | "/api/v1/authorization/roles" |
    | "/api/v1/authorization/roles/{roleId}" |
    | "/api/v1/authorization/users/{userId}/roles" |
    | "/api/v1/bridge/useractions/categories" |
    | "/api/v1/bridge/useractions/metadata" |
    | "/api/v1/bridge/useractions/metadata/{name}" |
#    | "/api/v1/certificate/details" | # only supports POST
    | "/api/v1/configuration/edgegroups" |
    | "/api/v1/configuration/edgegroups/{edgeGroupId}" |
    | "/api/v1/configuration/edges" |
    | "/api/v1/configuration/edges/certificateauthorities" |
    | "/api/v1/configuration/edges/certificateauthorities/{certificateId}" |
    | "/api/v1/configuration/edges/{edgeId}" |
    | "/api/v1/configuration/edges/{edgeId}/lines" |
    | "/api/v1/configuration/edges/{edgeId}/lines/{lineId}" |
    | "/api/v1/configuration/edges/{edgeId}/logicalinterfaces" |
#    | "/api/v1/configuration/edges/{edgeId}/logicalinterfaces/{interfaceId}" | # only supports PUT and DELETE
    | "/api/v1/configuration/edges/{edgeId}/physicalInterfaces/{interfaceId}" |
    | "/api/v1/configuration/edges/{edgeId}/physicalinterfaces" |
    | "/api/v1/configuration/edges/{edgeId}/softwareupdate" |
    | "/api/v1/configuration/edges/{edgeId}/softwareversions" |
    | "/api/v1/configuration/endpoints" |
    | "/api/v1/configuration/endpoints/{endpointId}" |
    | "/api/v1/configuration/languages" |
    | "/api/v1/configuration/linetemplates" |
    | "/api/v1/configuration/linetemplates/{templateId}" |
    | "/api/v1/configuration/organization" |
#    | "/api/v1/configuration/organizations" | # only supports POST
    | "/api/v1/configuration/organizations/{orgId}" |
    | "/api/v1/configuration/outboundroutes" |
    | "/api/v1/configuration/outboundroutes/{outboundRouteId}" |
    | "/api/v1/configuration/phones" |
    | "/api/v1/configuration/phones/{phoneInstanceId}" |
    | "/api/v1/configuration/phonetemplates" |
    | "/api/v1/configuration/phonetemplates/{phoneTemplateId}" |
    | "/api/v1/configuration/recordingkeys" |
    | "/api/v1/configuration/recordingkeys/rotationschedule" |
    | "/api/v1/configuration/retentionpolicies" |
    | "/api/v1/configuration/retentionpolicies/{policyId}" |
    | "/api/v1/configuration/schemas/edges/vnext" |
    | "/api/v1/configuration/schemas/edges/vnext/{schemaCategory}" |
    | "/api/v1/configuration/schemas/edges/vnext/{schemaCategory}/{schemaType}/{schemaId}" |
    | "/api/v1/configuration/securitypolicies" |
    | "/api/v1/configuration/securitypolicies/{policyId}" |
    | "/api/v1/configuration/sites" |
    | "/api/v1/configuration/sites/{siteId}" |
    | "/api/v1/configuration/sites/{siteId}/classifications" |
    | "/api/v1/configuration/sites/{siteId}/classifications/{classificationId}" |
    | "/api/v1/configuration/stations" |
    | "/api/v1/configuration/stations/{stationInstanceId}" |
    | "/api/v1/configuration/stationtemplates" |
    | "/api/v1/configuration/stationtemplates/{stationTemplateId}" |
    | "/api/v1/configuration/uservoicemailpolicies/{userId}" |
    | "/api/v1/configuration/voicemailpolicy" |
#    | "/api/v1/contentmanagement/auditquery" | # only supports POST
    | "/api/v1/contentmanagement/documents" |
    | "/api/v1/contentmanagement/documents/{documentId}" |
    | "/api/v1/contentmanagement/documents/{documentId}/audits" |
    | "/api/v1/contentmanagement/documents/{documentId}/content" |
    | "/api/v1/contentmanagement/query" |
    | "/api/v1/contentmanagement/status" |
    | "/api/v1/contentmanagement/status/{statusId}" |
    | "/api/v1/contentmanagement/workspaces" |
    | "/api/v1/contentmanagement/workspaces/{workspaceId}" |
    | "/api/v1/contentmanagement/workspaces/{workspaceId}/attributes" |
    | "/api/v1/contentmanagement/workspaces/{workspaceId}/attributes/{attributeId}" |
#    | "/api/v1/contentmanagement/workspaces/{workspaceId}/attributes/{attributeId}/instances" | # only supports POST
#    | "/api/v1/contentmanagement/workspaces/{workspaceId}/attributes/{attributeId}/instances/query" | # only supports POST
    | "/api/v1/contentmanagement/workspaces/{workspaceId}/attributes/{attributeId}/instances/{instanceId}" |
    | "/api/v1/contentmanagement/workspaces/{workspaceId}/members" |
    | "/api/v1/contentmanagement/workspaces/{workspaceId}/members/{memberId}" |
    | "/api/v1/contentmanagement/workspaces/{workspaceId}/tagvalues" |
#    | "/api/v1/contentmanagement/workspaces/{workspaceId}/tagvalues/query" | # only supports POST
    | "/api/v1/contentmanagement/workspaces/{workspaceId}/tagvalues/{tagId}" |
    | "/api/v1/conversations" |
#    | "/api/v1/conversations/query" | # only supports POST
#    | "/api/v1/conversations/query/{anchor}" | # only supports POST
    | "/api/v1/conversations/{conversationId}" |
#    | "/api/v1/conversations/{conversationId}/participants/{participantId}" | # only supports PUT
#    | "/api/v1/conversations/{conversationId}/participants/{participantId}/consult" | # only supports POST PUT DELETE
    | "/api/v1/conversations/{conversationId}/recordings" |
    | "/api/v1/conversations/{conversationId}/recordings/{recordingId}" |
    | "/api/v1/conversations/{conversationId}/recordings/{recordingId}/annotations" |
    | "/api/v1/conversations/{conversationId}/recordings/{recordingId}/annotations/{annotationId}" |
    | "/api/v1/date" |
#    | "/api/v1/diagnostics" | # only supports POST
#    | "/api/v1/diagnostics/trace" | # only supports POST
    | "/api/v1/evaluations/favoritetemplates" |
#    | "/api/v1/evaluations/favoritetemplates/{templateId}" | # only supports DELETE
    | "/api/v1/evaluations/templates" |
    | "/api/v1/evaluations/templates/{templateId}" |
    | "/api/v1/fax/documents" |
    | "/api/v1/fax/documents/{documentId}" |
    | "/api/v1/fax/documents/{documentId}/content" |
    | "/api/v1/fax/summary" |
    | "/api/v1/featuretoggles" |
    | "/api/v1/flows" |
#    | "/api/v1/flows/actions" | # only supports POST
    | "/api/v1/flows/{flowId}" |
    | "/api/v1/flows/{flowId}/publishedresults/{id}" |
    | "/api/v1/flows/{flowId}/versions" |
    | "/api/v1/flows/{flowId}/versions/{versionId}" |
    | "/api/v1/groups" |
    | "/api/v1/groups/{groupId}" |
    | "/api/v1/groups/{groupId}/members" |
    | "/api/v1/languages" |
    | "/api/v1/languages/{languageId}" |
    | "/api/v1/login" |
    | "/api/v1/notifications/subscriptions" |
#    | "/api/v1/notifications/subscriptions/{topicId}" | # only supports POST DELETE
    | "/api/v1/outbound/callabletimesets" |
    | "/api/v1/outbound/callabletimesets/{callableTimeSetId}" |
    | "/api/v1/outbound/callanalysisresponsesets" |
    | "/api/v1/outbound/callanalysisresponsesets/audio" |
    | "/api/v1/outbound/callanalysisresponsesets/{callAnalysisSetId}" |
    | "/api/v1/outbound/campaigns" |
    | "/api/v1/outbound/campaigns/{campaignId}" |
#    | "/api/v1/outbound/campaigns/{campaignId}/agents/{userId}" | # only supports PUT
#    | "/api/v1/outbound/campaigns/{campaignId}/callback/schedule" | # only supports POST
    | "/api/v1/outbound/campaigns/{campaignId}/stats" |
    | "/api/v1/outbound/contactlists" |
    | "/api/v1/outbound/contactlists/{contactListId}" |
    | "/api/v1/outbound/contactlists/{contactListId}/contacts/{contactId}" |
#    | "/api/v1/outbound/contactlists/{contactListId}/contacts" | # only supports POST
#    | "/api/v1/outbound/contactlists/{contactListId}/export" | # only supports POST
    | "/api/v1/outbound/contactlists/{contactListId}/importstatus" |
    | "/api/v1/outbound/contactlists/{contactListId}/{campaignId}/penetrationrate" |
    | "/api/v1/outbound/dnclists" |
    | "/api/v1/outbound/dnclists/{dncListId}" |
#    | "/api/v1/outbound/dnclists/{dncListId}/export" | # only supports POST
    | "/api/v1/outbound/dnclists/{dncListId}/importstatus" |
    | "/api/v1/outbound/previews" |
    | "/api/v1/outbound/previews/{previewId}" |
    | "/api/v1/outbound/schedules/campaigns" |
    | "/api/v1/outbound/schedules/campaigns/{campaignId}" |
    | "/api/v1/outbound/sequences" |
    | "/api/v1/outbound/sequences/{sequenceId}" |
    | "/api/v1/outbound/wrapupcodemappings" |
    | "/api/v1/quality/calibrations" |
    | "/api/v1/quality/calibrations/{calibrationId}" |
#    | "/api/v1/quality/conversations/{conversationId}/evaluations" | # only supports POST
    | "/api/v1/quality/conversations/{conversationId}/evaluations/{evaluationId}" |
#    | "/api/v1/quality/evaluations/query" | # only supports POST
#    | "/api/v1/quality/evaluations/scoring" | # only supports POST
    | "/api/v1/quality/evaluators/activity" |
    | "/api/v1/quality/forms" |
    | "/api/v1/quality/forms/{formId}" |
    | "/api/v1/quality/forms/{formId}/versions" |
    | "/api/v1/quality/publishedforms" |
    | "/api/v1/routing/queues" |
    | "/api/v1/routing/queues/{queueId}" |
    | "/api/v1/routing/queues/{queueId}/members" |
#    | "/api/v1/routing/queues/{queueId}/members/{memberId}" | # only supports PATCH PUT DELETE
    | "/api/v1/routing/queues/{queueId}/users" |
    | "/api/v1/routing/queues/{queueId}/wrapupcodes" |
    | "/api/v1/routing/skills" |
    | "/api/v1/routing/wrapupcodes" |
    | "/api/v1/routing/wrapupcodes/{codeId}" |
    | "/api/v1/scripter/favoritetemplates" |
#    | "/api/v1/scripter/favoritetemplates/{templateId}" | # only supports DELETE
    | "/api/v1/scripter/templates" |
    | "/api/v1/scripter/templates/{templateId}" |
    | "/api/v1/scripts" |
    | "/api/v1/scripts/published" |
    | "/api/v1/scripts/published/{scriptId}" |
    | "/api/v1/scripts/published/{scriptId}/pages" |
    | "/api/v1/scripts/published/{scriptId}/pages/{pageId}" |
#    | "/api/v1/scripts/templates" | # only supports POST
    | "/api/v1/scripts/{scriptId}" |
    | "/api/v1/scripts/{scriptId}/pages" |
    | "/api/v1/scripts/{scriptId}/pages/{pageId}" |
    | "/api/v1/sessions" |
    | "/api/v1/sessions/{sessionId}" |
    | "/api/v1/settings" |
    | "/api/v1/stations" |
    | "/api/v1/stations/{id}" |
    | "/api/v1/statuses" |
    | "/api/v1/timezones" |
    | "/api/v1/userrecordings" |
    | "/api/v1/userrecordings/summary" |
    | "/api/v1/userrecordings/{recordingId}" |
    | "/api/v1/userrecordings/{recordingId}/media" |
    | "/api/v1/users" |
    | "/api/v1/users/me" |
    | "/api/v1/users/{userId}" |
    | "/api/v1/users/{userId}/queues" |
#    | "/api/v1/users/{userId}/queues/{queueId}" | # only supports PATCH
    | "/api/v1/users/{userId}/roles" |
    | "/api/v1/users/{userId}/skills" |
    | "/api/v1/voicemail/mailbox" |
    | "/api/v1/voicemail/messages" |
    | "/api/v1/voicemail/messages/{messageId}" |
#    | "/health/check" | # never returns 401