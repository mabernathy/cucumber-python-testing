import os

def before_all(context):
    environment = os.getenv('ENV', 'DEV')
    context.properties = {}
    context.properties['publicapi_host'] = {
        'DEV' : 'https://public-api.us-east-1.inindca.com',
        'TEST' : 'https://public-api.us-east-1.inintca.com',
    }.get(environment, 'https://public-api.us-east-1.inindca.com')